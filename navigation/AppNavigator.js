import React from 'react';
import { createAppContainer, createSwitchNavigator ,createStackNavigator} from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/Login/Login';
import RegistrationScreen from '../screens/registration/Registration';

const AuthStack = createStackNavigator(
    { 
      Login: LoginScreen,
      Registration: RegistrationScreen 
    }
  );

export default createAppContainer(createSwitchNavigator(
  {
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: MainTabNavigator,
    Auth : AuthStack,
  },
  {
    initialRouteName: 'Auth',
  }
));