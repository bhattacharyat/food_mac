import React from "react";
import { Image, Button } from 'react-native-elements';
import {
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import { styles } from './styles';

export default class Registration extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      password: "",
      password_confirmation: ""
    };
  }

  static navigationOptions = {
    title: 'Sign In',
    headerStyle: {
      backgroundColor: '#3897f1',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  render() {
    return (

        <KeyboardAvoidingView style={styles.containerView} behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.registrationFormView}>
                <Image style={styles.logo} source={require("../../assets/images/food_mac.png")} />
                <TextInput
                        value={this.state.name}
                        onChangeText={name => this.setState({ name })}
                        style={styles.input}
                        placeholder="Name"
                        placeholderTextColor="#c4c3cb"
                        returnKeyType="next"
                        onSubmitEditing={() => this.emailInput.focus()}
                />
                <TextInput
                    value={this.state.email}
                    onChangeText={email => this.setState({ email })}
                    style={styles.input}
                    placeholderTextColor="#c4c3cb"
                    returnKeyType="next"
                    ref={input => (this.emailInput = input)}
                    onSubmitEditing={() => this.passwordCInput.focus()}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder="Email"
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={password => this.setState({ password })}
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#c4c3cb"
                    ref={input => (this.passwordCInput = input)}
                    onSubmitEditing={() => this.passwordInput.focus()}
                    returnKeyType="next"
                    secureTextEntry
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={password_confirmation => this.setState({ password_confirmation })}
                    style={styles.input}
                    placeholder="Confirm Password"
                    secureTextEntry={true}
                    placeholderTextColor="#c4c3cb"
                    returnKeyType="go"
                    secureTextEntry
                    ref={input => (this.passwordInput = input)}
                />
                <Button
                    buttonStyle={styles.signUpbutton}
                    onPress={() => this.onRegisterPress()}
                    title="Register"
                />
            </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );
  }

  onRegisterPress = () =>{
    if (!this.validateEmail(this.state.email) || this.state.password === "" || this.state.name === "") {
      Alert.alert("Please fill all required fields.");
    } else {
        this.props.navigation.navigate('Login');
    }
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };



}