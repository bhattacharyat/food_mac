import { StyleSheet } from 'react-native';

  export const styles = StyleSheet.create({
    containerView: {
      flex: 1,
    },
    registrationFormView: {
        flex:1,
    },
    logo:{
        marginTop: 25,
        marginBottom: 25,
        width: '100%', 
        height: 200,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    input: {
        height: 43,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#bec5d1',
        backgroundColor: '#fafafa',
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 5,
    },
    signUpbutton: {
        backgroundColor: '#3897f1',
        borderRadius: 5,
        height: 45,
        marginTop: 50,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 25,
    },
    buttonText: {
      fontSize: 18,
      alignSelf: "center",
      textAlign: "center",
      color: "#FFF",
      fontWeight: "700"
    },
    subtext: {
      color: "#ffffff",
      width: 160,
      textAlign: "center",
      fontSize: 35,
      fontWeight: "bold",
      marginTop: 20
    }
  });