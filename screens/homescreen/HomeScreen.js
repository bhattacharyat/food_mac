import React from 'react';
import { styles } from './styles';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  View,
  ImageEditor
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import t from 'tcomb-form-native';
import { Image, Button } from 'react-native-elements';
import { Constants, Permissions , ImagePicker} from 'expo';
import { createNewTicketInDB } from '../../connection/Tickets';

const Form = t.form.Form;

const Email = t.refinement(t.String, email => {
  const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/; //or any other regexp
  return reg.test(email);
});

const CustomerForm = t.struct({
  name_of_the_company: t.String,
  name: t.String,
  position: t.String,
  email: Email,
  phone: t.Number,
  machine_model: t.String,
  machine_serial : t.String,
  description_of_problem: t.String,
  parts_required: t.maybe(t.String),
});


const options = {
  fields: {
    name_of_the_company :{
      error: 'Required',
      label: 'Name of the company *',
      placeholder: 'Name of the company',
      returnKeyType: 'next'
    },
    name :{
      error: 'Required',
      label: 'Name *',
      placeholder: 'Name',
      returnKeyType: 'next'
    },
    position: {
      error: 'Required',
      label: 'Designation *',
      placeholder: 'Designation',
      returnKeyType: 'next'
    },
    email: {
      error: 'Required',
      label: 'Email *',
      placeholder: 'Email',
      keyboardType: 'email-address',
      returnKeyType: 'next'
    },
    phone: {
      error: 'Required',
      label: 'Phone *',
      placeholder: 'Phone',
      keyboardType: 'phone-pad',
      returnKeyType: 'next'
    },
    machine_model: {
      error: 'Required',
      label: 'Machine Model *',
      placeholder: 'Machine Model',
      returnKeyType: 'next'
    },
    machine_serial: {
      error: 'Required',
      label: 'Machine Serial *',
      placeholder: 'Machine Serial',
      returnKeyType: 'next'
    },
    description_of_problem: {
      error: 'Required',
      label: 'Description of the problem *',
      placeholder: 'Description of the problem',
      returnKeyType: 'next'
    },
    parts_required: {
      label: 'Parts Required ',
      placeholder: 'Parts Required',
      returnKeyType: 'done'
    },
  },
};


export default class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'Add New Form',
    headerStyle: {
      backgroundColor: '#3897f1',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  constructor(props) {
    super(props)
    this.state = {
      value: {},
      cameraimagesource: null,
      imageheight: 200,
      imagewidth: 0
    }
  }
  
  handleSubmit = () => {
    const value = this.formRef.getValue();
    createNewTicketInDB(value);
    //Navigate to Track page from here
  }

  componentDidMount() {
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL); 
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }else{
        this.setState({ hasCameraPermission: status === 'granted' });
      }
    }else {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }else{
        this.setState({ hasCameraPermission: status === 'granted' });
      }
    }
  }


  pickFromCamera = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    
    if (result.cancelled) {
      console.log('got here');
      return;
    }

    let resizedUri = await new Promise((resolve, reject) => {
      ImageEditor.cropImage(result.uri,
        {
          offset: { x: 0, y: 0 },
          size: { width: result.width, height: result.height },
          displaySize: { width: this.state.imagewidth, height: this.state.imageheight },
          resizeMode: 'contain',
        },
        (uri) => resolve(uri),
        () => reject(),
      );
    });
    
    this.setState({ cameraimagesource: resizedUri });
  };

 
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Form ref={c => this.formRef = c} 
            type={CustomerForm} 
            value={this.state.value}
            options={options}
           />
            <View  
            onLayout={(event) => { this.setState({ imagewidth: event.nativeEvent.layout.width }); }}
            style={styles.imageContainer}>
                <Image
                  source={(this.state.cameraimagesource) ? { uri: this.state.cameraimagesource } : require('../../assets/images/camera.png')} 
                  style={{ width: this.state.imagewidth, height: this.state.imageheight }}
                  resizeMode="contain"
                />
            </View >
        
            <Button
                  buttonStyle={styles.submitBtn}
                  onPress={() => this.pickFromCamera()}
                  title="Take Picture"
            />

            <Button
                  buttonStyle={styles.submitBtn}
                  onPress={() => this.handleSubmit()}
                  title="Submit"
            />
        </ScrollView>
      </View>
    );
  }

}
