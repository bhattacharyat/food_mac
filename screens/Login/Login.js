import React from 'react';
import { Image, Button } from 'react-native-elements';
import { styles } from './styles';
import {Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView} from 'react-native';

export default class Login extends React.Component {

    constructor(props){
        super(props);
        this.state={
            isLoading: false,
            emailId : "",
            password : "",
        }
    }

    static navigationOptions = {
        headerStyle: {
          backgroundColor: '#3897f1',
        },
    };

    render() {
      return (
        <KeyboardAvoidingView style={styles.containerView} behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.loginScreenContainer}>
            <Image
                    source={require('../../assets/images/food_mac.png')}
                    style={styles.logo}
                />
                <View style={styles.loginFormView}>
                <TextInput placeholder="Email Id" 
                    placeholderColor="#c4c3cb" 
                    style={styles.loginFormTextInput} 
                    onChangeText={value => this.setState({emailId: value.trim()})}
                />
                <TextInput placeholder="Password" 
                    placeholderColor="#c4c3cb" 
                    style={styles.loginFormTextInput} 
                    secureTextEntry={true}
                    onChangeText={value => this.setState({password: value.trim()})}
                />
                <Button
                    loading={this.state.isLoading}
                    buttonStyle={styles.loginButton}
                    onPress={() => this.onLoginButtonClick()}
                    title="Login"
                />
                
                    <View style={styles.register}>                        
                        <Text>Not yet registered?</Text>
                        <Text style={styles.registerText} onPress={this.onRegisterClick}>Register Now</Text>
                    </View>
                </View>
            </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      );
    }
  
    componentDidMount() {
    }
  
    componentWillUnmount() {
    }
  
    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    
    onLoginButtonClick = () => {
        if (!this.validateEmail(this.state.emailId) || this.state.password === "") {
            Alert.alert("Please enter valid credential.");
        } else {
            this.props.navigation.navigate('Main');
        }
        
    }

    onRegisterClick = () => {
        this.props.navigation.navigate('Registration');
    }

  }