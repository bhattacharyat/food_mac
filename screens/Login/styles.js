import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
    container: {
     flex: 1,
     justifyContent: 'center'
    },
    logo:{
        marginTop: 25,
        marginBottom: 25,
        width: '100%', 
        height: 200,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    login:{
      width: '40%',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginVertical: 20
    },
    register:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 25,
    },
    registerText:{
        color: 'blue',
        marginLeft: 10
    },
    containerView: {
        flex: 1,
      },
      loginScreenContainer: {
        flex: 1,
      },
      logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 150,
        marginBottom: 30,
        textAlign: 'center',
      },
      loginFormView: {
        flex: 1
      },
      loginFormTextInput: {
        height: 43,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#bec5d1',
        backgroundColor: '#fafafa',
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 5,
      
      },
      loginButton: {
        backgroundColor: '#3897f1',
        borderRadius: 5,
        height: 45,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 25,
      },

  })