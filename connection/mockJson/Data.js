export const ticketData =   [{
    ticketId: 'tick01',
    name_of_the_company :'The ABC',
    name :'Mr. Test Test',
    position: 'Tikcet logger',
    email: 'test.test@ticketlogger.com',
    phone: '934748393',
    machine_model: 'FPROCESS2019',
    machine_serial: 'FX933992BD23',
    description_of_problem: 'Product not working as expected',
    parts_required: 'New filter',
    image:''
  },
  {
    ticketId: 'tick02',
    name_of_the_company :'XYZ Company',
    name :'Mr. Demo Demo',
    position: 'Tikcet logger',
    email: 'test.test@demo.com',
    phone: '93474873783',
    machine_model: 'FPROCESS2019',
    machine_serial: 'FX933992BD26',
    description_of_problem: 'Product not working as expected',
    parts_required: 'New filter',
    image:''
  }];