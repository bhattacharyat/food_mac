import {AsyncStorage} from 'react-native';
import { TICKETKEY } from '../constants/AppConstants';
import { ticketData } from './mockJson/Data';

export const createNewTicketInDB = async formData => {
    //Call web service when available
    formData.ticketId = generateTicketNumber();
    const data = await AsyncStorage.getItem(TICKETKEY);
    const existing = JSON.parse(data);
    existing.push(formData);
    AsyncStorage.setItem(TICKETKEY, JSON.stringify(existing));

    const data1 = await AsyncStorage.getItem(TICKETKEY);
    console.log('Data ', data1);
}

export const pullTicketsFromDB = async () => {
    //Call web service when available
    await AsyncStorage.setItem(TICKETKEY, JSON.stringify(ticketData));
}

generateTicketNumber = () =>{
    return 'tick'+ Math.floor(Math.random() *1000);
}

export const getAllTickets = async () => {
    const data = await AsyncStorage.getItem(TICKETKEY);
    return data;
}